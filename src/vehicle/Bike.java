/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehicle;

/**
 *
 * @author Akshay Reddy Vontari
 */
public class Bike extends TwoWheeler{
    private String name;

    /**Bike constructor with no-args
     *
     */
    public Bike() 
    {
        
    }

    /**Bike Constructor with two args
     *
     * @param manufacturerName The name of vehicle manufacturer
     * @param v_Id The ID of the Vehicle
     */
    public Bike(String manufacturerName, int v_Id) {
        super(manufacturerName, v_Id);
        this.name = "Bike";
    }

    /**Returns the string representation of a variable or operation
     * 
     * @return Returns the operation when we call toString method in main method
     */
    @Override
    public String toString() {
        
        return "Two Wheeler Details:\nManufacturer Name:" + getManufacturerName() + "\nVehicle Id:" + getV_Id()+super.toString();
    }
    
    
    
}
