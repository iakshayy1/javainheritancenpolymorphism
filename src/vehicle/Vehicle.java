/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehicle;

/**
 *
 * @author Akshay Reddy Vontari
 */
public class Vehicle {
    private String manufacturerName;
    private int v_Id;

    /**
     * Vehicle Constructor with no-args
     *
     */
    public Vehicle() 
    {
        
    }

    /**Vehicle constructor with two args
     *
     * @param manufacturerName The name of vehicle manufacturer
     * @param v_Id The ID of the Vehicle
     */
    public Vehicle(String manufacturerName, int v_Id)
    {
        this.manufacturerName = manufacturerName;
        this.v_Id = v_Id;
    }

    /**when we call getters in main method it gives return type
     *
     * @return the name of vehicle manufacturer
     */
    public String getManufacturerName()
    {
        return manufacturerName;
    }

    /**When we call setters in the main method it sets the value.
     *
     * @param manufacturerName sets the name of vehicle manufacturer
     */
    public void setManufacturerName(String manufacturerName)
    {
        this.manufacturerName = manufacturerName;
    }

    /**when we call getters in main method it gives return type
     *
     * @return the ID of the vehicle
     */
    public int getV_Id()
    {
        return v_Id;
    }

    /**When we call setters in the main method it sets the value.
     *
     * @param v_Id the sets the ID of the vehicle
     */
    public void setV_Id(int v_Id)
    {
        this.v_Id = v_Id;
    }
    /**Returns the string representation of a variable or operation
     * 
     * @return Returns the operation when we call toString method in main method
     */
    @Override
    public String toString() 
    {
        return "Vehicle Details:\nManufacturerName:" + manufacturerName + " Vehicle Id:" + v_Id;
    }
    
    
    
    
    
}
