/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehicle;

/**
 *
 * @author Akshay Reddy Vontari
 */
public class Car extends FourWheeler {
    private String name;
    private long initialDistance;
    private long finalDistance;
    private double initialGasQuantity;
    private double finalGasQuantity;

    /**Car constructor with no-args
     *
     */
    public Car() {
        super();
    }

    /**Car constructor with six args
     *
     * @param initialDistance The initial reading of the vehicle
     * @param finalDistance The final reading of the vehicle
     * @param initialGasQuantity The initial gas quantity available
     * @param finalGasQuantity The final gas quantity available
     * @param manufacturerName The name of manufacturer of vehicle
     * @param v_Id The ID of the vehicle
     */
    public Car(long initialDistance, long finalDistance, double initialGasQuantity, double finalGasQuantity, String manufacturerName, int v_Id) {
        super(manufacturerName, v_Id);
        this.name = "Car";
        this.initialDistance = initialDistance;
        this.finalDistance = finalDistance;
        this.initialGasQuantity = initialGasQuantity;
        this.finalGasQuantity = finalGasQuantity;
    }

    /**on calling method in main method gives return type
     *
     * @return the gas consumed to cover the distance by vehicle in gallons
     */
    public double calculateGas(){
        return (initialGasQuantity-finalGasQuantity) * 0.264179853644361;
        }

    /**on calling method in main method gives return type
     *
     * @return the distance traveled by vehicle in miles
     */
    public double calculateDistance(){
        return (finalDistance - initialDistance) *  0.6214;
        }

    /**on calling method in main method gives return type
     *
     * @return The mileage of the vehicle
     */
    public double calculateMileage(){
        return calculateDistance()/calculateGas();
    }
    /**Returns the string representation of a variable or operation
     * 
     * @return Returns the operation when we call toString method in main method
     */
    @Override
    public String toString() {
        return "Four Wheeler Details:\nManufacturer Name:" + getManufacturerName() + "\nVehicle Id:" + getV_Id()+"\nType of Vehicle:" + name + "\nThe distance travelled is:" + this.calculateDistance()+ "\nThe gas consumed to cover the distance:" + this.calculateGas() + "\nThe Mileage of the vehicle is:"+this.calculateMileage();
    }
    
    
    
    
    
    
    
}
