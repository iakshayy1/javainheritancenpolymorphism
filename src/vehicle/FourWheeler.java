/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehicle;

import java.util.ArrayList;

/**
 *
 * @author Akshay Reddy Vontari
 */
public class FourWheeler extends Vehicle {
    private ArrayList<String> accessories = new ArrayList<String>();

    /**
     * FourWheeler constructor with no-args
     */
    public FourWheeler() {
        super();
    }
   
    /**
     * FourWheeler constructor with two-args
     * @param manufacturerName The name of vehicle manufacturer
     * @param v_Id The ID of the Vehicle
     */
    public FourWheeler(String manufacturerName, int v_Id) {
        super(manufacturerName, v_Id);
        this.accessories= accessories;
    }

    /**when we call getters in main method it gives return type
     *
     * @return the all the accessories added to the list
     */
    public ArrayList<String> getAccessories() {
        return accessories;
    }

    /**When we call setters in the main method it sets the value.
     *
     * @param accessories sets the accessories to the list
     */
    public void setAccessories(ArrayList<String> accessories) {
        this.accessories = accessories;
    }

    /**on calling method in main method gives return type
     *
     * @param items accessories listed as items
     * @return the accessories which are added to the list
     */
    public ArrayList<String> addAccessories(String items)
    {
     
         String[] split = items.split(",");
         for(String element: split){
             accessories.add(element);
         }
        
     
        
        return accessories;
    }
    
/**Returns the string representation of a variable or operation
     * 
     * @return Returns the operation when we call toString method in main method
     */
    @Override
    public String toString() {
        return "Four Wheeler Details:\n"+ super.toString();
    }

  
    
    
}
