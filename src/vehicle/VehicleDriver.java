/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehicle;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


/**
 *
 * @author Akshay Reddy Vontari
 */
public class VehicleDriver {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException {
        // TODO code application ogic here
        Scanner sc = new Scanner(new File("vehicle.txt"));
        String ManuName = sc.nextLine();
        int Id = Integer.parseInt(sc.nextLine());
        Vehicle V = new Vehicle(ManuName,Id);
        System.out.println(V.toString());
        System.out.println("*********************************************************");
        while(sc.hasNext())
        {
            String TwoOrFour = sc.nextLine();
            if(TwoOrFour.equalsIgnoreCase("Four Wheeler"))
            { 
                String CarOrTruck = sc.nextLine();
                if(CarOrTruck.equalsIgnoreCase("car"))
                {
                 Long initRead = sc.nextLong();
                 Long finlRead = sc.nextLong();
                 Double initDist = sc.nextDouble();
                 Double finlDist = sc.nextDouble();
                 Car c = new Car(initRead,finlRead,initDist,finlDist,ManuName,Id); 
                 System.out.println(c.toString());
                 System.out.println("*********************************************************");
                }
                else if(CarOrTruck.equalsIgnoreCase("Truck"))
                {
                 Truck t = new Truck("Maserati",12345);
                 String acss = sc.nextLine();
                 // System.out.println(acss);
                 t.addAccessories(acss);
                 t.calculateExtraFittingCost();
                 System.out.println(t.toString());
                 System.out.println("*********************************************************");
                    
                }
            }
             else if(TwoOrFour.equalsIgnoreCase("TwoWheeler"))
            {
                String y = sc.nextLine();
                //System.out.println(y);
                int z = Integer.parseInt(sc.nextLine());
                //System.out.println(z);
               
                //According to polymorphic substitution,because of super class and subclass relationship the object of subclass can access superclass accessiblity this defines reference variable for supertype may actually store a reference to instance of the subclass 

                TwoWheeler b = new Bike(y,z);
                String x = sc.nextLine();
                b.Identifier(x);
                System.out.println(b.toString());
                System.out.println("*********************************************************");
                
                
            }
           
            
        }
        
        System.out.println("Congratulations,End of the program!");    
            
    }
        
            
}
        
    
    
