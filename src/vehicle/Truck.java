/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehicle;


/**
 *
 * @author Akshay Reddy Vontari
 */
public class Truck extends FourWheeler {
    private String name;
    private final double BASE_PRICE=30;

    /**Truck constructor with no-args
     *
     */
    public Truck() {
        super();
    }

    /**Truck constructor with two-args
     *
     * @param manufacturerName The name of vehicle manufacturer
     * @param v_Id The ID of the Vehicle
     */
    public Truck(String manufacturerName, int v_Id) {
        super(manufacturerName, v_Id);
        this.name = "Truck";
    }
    
    /**on calling method in main method gives return type
     *
     * @return the total extra fitting cost of the vehicle
     */
    public double calculateExtraFittingCost(){
        double i=0;
        double totalPrice=0;
        for(String v: super.getAccessories())
        {
        switch(v)
        {
            case "Floor Mats":
                i=i+39;
                break;
            case "Side Covers":
                i=i+50;
                break;
            case "Headlights":
                i=i+90;
                break;
            case "Custom Grilles":
                i=i+101;
                break;
            case "Audio System":
                i=i+70;
                break;
                
         }
       
    }
        totalPrice= BASE_PRICE+i;
        return totalPrice;
}

    /**Returns the string representation of a variable or operation
     * 
     * @return Returns the operation when we call toString method in main method
     */
    @Override
    public String toString() {
        return "Four Wheeler Details:\nManufacturer Name:" + getManufacturerName() + "\nVehicle Id:" + getV_Id()+"\nType of Vehicle:"+name+"\nThe total cost of Extra Interiors is:"+calculateExtraFittingCost();
    }
    
    
    
    
}
