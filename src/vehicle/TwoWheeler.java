/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehicle;

/**
 *
 * @author Akshay Reddy Vontari
 */
public class TwoWheeler extends Vehicle{
    private String message;

    /**TwoWheeler constructor with no-args
     *
     */
    public TwoWheeler()
    {
        
    }

    /**TwoWheeler constructor with two-args
     *
     * @param manufacturerName The name of vehicle manufacturer
     * @param v_Id The ID of the Vehicle
     */
    public TwoWheeler(String manufacturerName, int v_Id)
    {
        super(manufacturerName, v_Id);
        this.message=null;
    }

    /**when we call getters in main method it gives return type
     *
     * @return the message whether it is super bike or a cycle
     */
    public String getMessage() 
    {
        return message;
    }
    
    /**on calling method in main method gives return type
     *
     * @param category defines the category of the vehicle
     * @return the message whether it is super bike or a cycle
     */
    public String Identifier(String category)
    {
        if(category.equalsIgnoreCase("petrol"))
        {
            message= "is a super bike";
        }
        else
        {
            message= "is a cycle";
        }
        return message;
    }
    
     /**Returns the string representation of a variable or operation
     * 
     * @return Returns the operation when we call toString method in main method
     */
    @Override
    public String toString()
    {
        return  "\nThe TwoWheeler " +message;
    }
    
    

    

    
    
    
    
    
    
    
    
}
